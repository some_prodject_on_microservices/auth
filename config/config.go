package config

import (
	"time"

	"github.com/spf13/viper"
)

type Config struct {
	Release    bool
	AppName    string
	GrpcPort   string
	LogDir     string
	LogFile    string
	SecretKey  string
	SigningKey string
	Ttl        time.Duration
	DbLogin    string
	DbPass     string
	DbHost     string
	DbPort     string
	DbName     string
	DbArgs     string
}

// InitConfig - load config from config.yml
func InitConfig() Config {
	viper.AddConfigPath("./config")
	viper.SetConfigName("config")

	err := viper.ReadInConfig()
	if err != nil {
		panic(err)
	}

	conf := Config{
		Release:    viper.GetBool("release"),
		AppName:    viper.GetString("name"),
		GrpcPort:   viper.GetString("grpc_port"),
		LogDir:     viper.GetString("log.dir"),
		LogFile:    viper.GetString("log.file"),
		SecretKey:  viper.GetString("auth.secret_key"),
		SigningKey: viper.GetString("auth.signing_key"),
		Ttl:        viper.GetDuration("auth.ttl"),
		DbLogin:    viper.GetString("db.login"),
		DbPass:     viper.GetString("db.pass"),
		DbHost:     viper.GetString("db.host"),
		DbPort:     viper.GetString("db.port"),
		DbName:     viper.GetString("db.name"),
		DbArgs:     viper.GetString("db.args"),
	}

	return conf
}
