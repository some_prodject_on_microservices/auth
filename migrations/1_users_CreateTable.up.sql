CREATE TABLE IF NOT EXISTS `users` ( 
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT, 
  `email` VARCHAR(20) NOT NULL UNIQUE,
  `pass` VARCHAR(100) NOT NULL,
  PRIMARY KEY `pk_id`(`id`)
);