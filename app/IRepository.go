package app

import (
	"context"

	"gitlab.com/some_prodject_on_microservices/auth/models"
)

// Repository ...
type Repository interface {
	CheckEmailExist(ctx context.Context, email string) bool
	CreateUser(ctx context.Context, user models.User) error
	GetUser(ctx context.Context, email string) (models.User, error)

	Close() error
}
