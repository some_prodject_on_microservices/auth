package grpc

import (
	"context"
	"strconv"

	pb "gitlab.com/some_prodject_on_microservices/api/auth"
	"gitlab.com/some_prodject_on_microservices/auth/app"
	"gitlab.com/some_prodject_on_microservices/auth/models"
)

type Handler struct {
	uc app.Usecase
	pb.UnimplementedAuthServer
}

func NewHandler(uc app.Usecase) *Handler {
	return &Handler{
		uc: uc,
	}
}

func (h *Handler) SignUp(ctx context.Context, user *pb.User) (*pb.AuthResult, error) {
	token, err := h.uc.SignUp(ctx, h.toModelUser(user))
	if err != nil {
		return nil, err
	}

	return &pb.AuthResult{Token: token}, nil
}

func (h *Handler) SignIn(ctx context.Context, user *pb.User) (*pb.AuthResult, error) {
	token, err := h.uc.SignIn(ctx, h.toModelUser(user))
	if err != nil {
		return nil, err
	}

	return &pb.AuthResult{Token: token}, nil

}

func (h *Handler) ParseToken(ctx context.Context, input *pb.InputToken) (*pb.User, error) {
	mUser, err := h.uc.ParseToken(input.Token)
	if err != nil {
		return nil, err
	}

	return h.toPbUser(mUser), nil
}

func (h *Handler) toModelUser(user *pb.User) models.User {
	return models.User{
		Email: user.Email,
		Pass:  user.Pass,
	}
}

func (h *Handler) toPbUser(user models.User) *pb.User {
	return &pb.User{
		ID:    strconv.FormatUint(user.ID, 10),
		Email: user.Email,
		Pass:  user.Pass,
	}
}
