package app

import (
	"context"

	"gitlab.com/some_prodject_on_microservices/auth/models"
)

type Usecase interface {
	SignUp(ctx context.Context, user models.User) (string, error)
	SignIn(ctx context.Context, user models.User) (string, error)
	ParseToken(accessToken string) (models.User, error)
}
