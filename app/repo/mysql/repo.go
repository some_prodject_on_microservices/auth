package mysql

import (
	"context"
	"database/sql"

	"github.com/jmoiron/sqlx"
	"gitlab.com/some_prodject_on_microservices/auth/app"
	"gitlab.com/some_prodject_on_microservices/auth/models"
)

type mysqlrepo struct {
	db *sqlx.DB
}

func NewMySQLRepo(db *sql.DB) app.Repository {
	return &mysqlrepo{
		db: sqlx.NewDb(db, "mysql"),
	}
}

func (r *mysqlrepo) CheckEmailExist(ctx context.Context, email string) bool {
	var (
		id    uint64
		query string
		err   error
	)

	query = `SELECT id FROM users WHERE email = ?`
	err = r.db.GetContext(ctx, &id, query, email)
	if err != nil {
		return false
	}

	if id == 0 {
		return false
	}

	return true
}

func (r *mysqlrepo) CreateUser(ctx context.Context, user models.User) error {
	var (
		query string
		err   error
	)

	query = `INSERT INTO users SET
						email=:email,
						pass=:pass`

	_, err = r.db.NamedExecContext(ctx, query, r.toDbUser(user))
	if err != nil {
		return err
	}

	return nil
}

func (r *mysqlrepo) GetUser(ctx context.Context, email string) (models.User, error) {
	var (
		user  dbUser
		query string
		err   error
	)

	query = `SELECT * FROM users WHERE email = ?`

	err = r.db.GetContext(ctx, &user, query, email)
	if err != nil {
		return models.User{}, err
	}

	return r.toModelsUser(user), nil
}

func (r *mysqlrepo) Close() error {
	return r.db.Close()
}
