package mysql

import "gitlab.com/some_prodject_on_microservices/auth/models"

type dbUser struct {
	ID    uint64 `db:"id"`
	Email string `db:"email"`
	Pass  string `db:"pass"`
}

func (r *mysqlrepo) toDbUser(user models.User) dbUser {
	return dbUser{
		ID:    user.ID,
		Email: user.Email,
		Pass:  user.Pass,
	}
}

func (r *mysqlrepo) toModelsUser(user dbUser) models.User {
	return models.User{
		ID:    user.ID,
		Email: user.Email,
		Pass:  user.Pass,
	}
}
