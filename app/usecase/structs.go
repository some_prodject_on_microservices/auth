package usecase

import (
	"github.com/dgrijalva/jwt-go/v4"
	"gitlab.com/some_prodject_on_microservices/auth/models"
)

type claimsUser struct {
	ID    uint64
	Email string
}

func (u *usecase) toClaimsUser(user models.User) claimsUser {
	return claimsUser{
		ID:    user.ID,
		Email: user.Email,
	}
}

func (u *usecase) toModelsUser(user claimsUser) models.User {
	return models.User{
		ID:    user.ID,
		Email: user.Email,
	}
}

type AuthClaims struct {
	jwt.StandardClaims

	User claimsUser `json:"user"`
}
