package usecase

import (
	"context"
	"errors"
	"fmt"
	"time"

	"github.com/dgrijalva/jwt-go/v4"
	"gitlab.com/some_prodject_on_microservices/auth/app"
	"gitlab.com/some_prodject_on_microservices/auth/models"
	"golang.org/x/crypto/bcrypt"
)

type usecase struct {
	repo           app.Repository
	secret         string
	signingKey     []byte
	expireDuration time.Duration
}

func NewUsecase(repo app.Repository, secret string, signingKey []byte, expireDuration time.Duration) app.Usecase {
	return &usecase{
		repo:           repo,
		secret:         secret,
		signingKey:     signingKey,
		expireDuration: expireDuration,
	}
}

func (u *usecase) SignUp(ctx context.Context, user models.User) (string, error) {
	if u.repo.CheckEmailExist(ctx, user.Email) {
		return "", errors.New(AuthErr_EmailExist)
	}

	hash, err := bcrypt.GenerateFromPassword([]byte(user.Pass), 14)
	if err != nil {
		return "", err
	}

	user.Pass = string(hash)

	err = u.repo.CreateUser(ctx, user)
	if err != nil {
		return "", err
	}

	return u.generateToken(u.toClaimsUser(user))
}

func (u *usecase) SignIn(ctx context.Context, user models.User) (string, error) {
	dbUser, err := u.repo.GetUser(ctx, user.Email)
	if err != nil {
		return "", err
	}

	err = bcrypt.CompareHashAndPassword([]byte(dbUser.Pass), []byte(user.Pass))
	if err != nil {
		return "", errors.New(AuthErr_Login)
	}

	return u.generateToken(u.toClaimsUser(dbUser))
}

func (u *usecase) generateToken(user claimsUser) (string, error) {
	var (
		token    *jwt.Token
		strToken string
		err      error
	)

	claims := AuthClaims{
		User: user,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: jwt.At(time.Now().Add(u.expireDuration * time.Second)),
		},
	}

	token = jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	strToken, err = token.SignedString(u.signingKey)
	if err != nil {
		/*
			micrologger.LogError(
				"",
				fmt.Sprint("GenerateToken | ", AuthErr_CreateToken.Error()),
				"auth/usecase",
				fmt.Sprint("user name: ", user.Name),
				err,
			)*/
		return "", errors.New(AuthErr_CreateToken)
	}

	return strToken, nil
}

func (u *usecase) ParseToken(accessToken string) (models.User, error) {
	token, err := jwt.ParseWithClaims(accessToken, &AuthClaims{}, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}
		return u.signingKey, nil
	})
	if err != nil {
		return models.User{}, err
	}

	if claims, ok := token.Claims.(*AuthClaims); ok && token.Valid {
		return u.toModelsUser(claims.User), nil
	}

	return models.User{}, nil
}
