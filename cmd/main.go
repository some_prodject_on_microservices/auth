package main

import (
	"fmt"
	"os"
	"os/signal"

	"gitlab.com/some_prodject_on_microservices/auth/cmd/grpcserver"
	"gitlab.com/some_prodject_on_microservices/auth/config"
	"gitlab.com/some_prodject_on_microservices/auth/pkg/logger"
)

func main() {
	conf := config.InitConfig()
	logger.InitLogger(conf)

	grpc := grpcserver.NewApp(conf)
	go grpc.Run()

	fmt.Println(
		fmt.Sprintf(
			"Service %s is running",
			conf.AppName,
		),
	)

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt, os.Interrupt)

	<-quit

	grpc.Stop()
	
	fmt.Println(
		fmt.Sprintf(
			"Service %s is stopped",
			conf.AppName,
		),
	)
}
