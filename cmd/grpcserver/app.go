package grpcserver

import (
	"fmt"
	"net"

	mysql "github.com/DarkSoul94/dbconnectors/MySQL"
	pb "gitlab.com/some_prodject_on_microservices/api/auth"
	"gitlab.com/some_prodject_on_microservices/auth/app"
	appgrpc "gitlab.com/some_prodject_on_microservices/auth/app/delivery/grpc"
	apprepo "gitlab.com/some_prodject_on_microservices/auth/app/repo/mysql"
	appusecase "gitlab.com/some_prodject_on_microservices/auth/app/usecase"
	"gitlab.com/some_prodject_on_microservices/auth/config"

	"google.golang.org/grpc"
)

type Deps struct {
	AuthHandler pb.AuthServer
}

type App struct {
	Deps

	Conf       config.Config
	Repo       app.Repository
	grpcServer *grpc.Server
}

func NewApp(conf config.Config) *App {
	db, err := mysql.InitMysqlDB(
		conf.DbLogin,
		conf.DbPass,
		conf.DbHost,
		conf.DbPort,
		conf.DbName,
		conf.DbArgs,
		"file://migrations",
	)
	if err != nil {
		panic(err)
	}

	repo := apprepo.NewMySQLRepo(db)
	uc := appusecase.NewUsecase(
		repo,
		conf.SecretKey,
		[]byte(conf.SigningKey),
		conf.Ttl,
	)

	return &App{
		Deps: Deps{
			AuthHandler: appgrpc.NewHandler(uc),
		},
		Conf:       conf,
		grpcServer: grpc.NewServer(),
	}
}

func (a *App) Run() {
	l, err := net.Listen("tcp", fmt.Sprintf("0.0.0.0:%s", a.Conf.GrpcPort))
	if err != nil {
		panic(err)
	}

	pb.RegisterAuthServer(a.grpcServer, a.AuthHandler)

	err = a.grpcServer.Serve(l)
	if err != nil {
		panic(err)
	}
}

func (a *App) Stop() {
	a.grpcServer.Stop()
	a.Repo.Close()
}
